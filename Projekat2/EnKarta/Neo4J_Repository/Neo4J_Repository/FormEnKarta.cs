﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using Neo4J_Repository.DomainModel;
using System.IO;

namespace Neo4J_Repository
{
    public partial class FormEnKarta : Form
    {
        private GraphClient client;
        private Stavka cvor;
        public FormEnKarta()
        {
            InitializeComponent();
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            //string naziv = ".*" + tbNaziv.Text + ".*";

            //Dictionary<string, object> queryDict = new Dictionary<string, object>();
            //queryDict.Add("naziv", naziv);

            //var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Karta) and exists(n.naziv) and n.naziv =~ {naziv} return n",
            //                                                queryDict, CypherResultMode.Set);

            //List<Stavka> stavke = ((IRawGraphClient)client).ExecuteGetCypherResults<Stavka>(query).ToList();

            //if (stavke.Count != 0)
            //{
            //    foreach (Stavka s in stavke)
            //    {
            //        //DateTime bday = a.getBirthday();
            //        //MessageBox.Show(s.naziv);
            //        lblNaziv.Text = s.naziv;
            //        rtbOpis.Text = s.opis;
            //        var pic = Convert.FromBase64String(s.slika);

            //        using (MemoryStream ms = new MemoryStream(pic))
            //        {
            //            pictureBoxSlika.Image = Image.FromStream(ms);
            //        }
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Enciklopedija ne sadrzi trazeni pojam!", "Obavesteje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            groupBoxKomsije.Controls.Clear();
            PronadjiCvor(tbNaziv.Text);
            PronadjiKomsije(tbNaziv.Text);
            PrikaziCvor();
            PrikaziKomsije();
            tbNaziv.Clear();
        }

        private void PronadjiCvor(String naziv)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("naziv", naziv);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Karta) and exists(n.naziv) and n.naziv =~ {naziv} return n",
                                                            queryDict, CypherResultMode.Set);

            List<Stavka> stavke = ((IRawGraphClient)client).ExecuteGetCypherResults<Stavka>(query).ToList();

            if (stavke.Count != 0)
            {
                foreach (Stavka s in stavke)
                {
                    cvor.naziv = s.naziv;
                    cvor.slika = s.slika;
                    cvor.opis = s.opis;
                }
            }
            else
            {
                MessageBox.Show("Enciklopedija ne sadrzi trazeni pojam!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void PronadjiKomsije(String naziv)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("naziv", naziv);

            var query = new Neo4jClient.Cypher.CypherQuery("match (x:Karta),(y:Karta) where x.naziv =~ {naziv} and (x)-->(y) return y",
                                                            queryDict, CypherResultMode.Set);

            List<Stavka> stavke = ((IRawGraphClient)client).ExecuteGetCypherResults<Stavka>(query).ToList();
            cvor.komsije.Clear();
            if (stavke.Count != 0)
            {
                foreach (Stavka s in stavke)
                {
                    cvor.komsije.Add(s);
                }
            }

        }
        private void PrikaziCvor()
        {
            if (cvor.naziv != null)
            {
                lblNaziv.Text = cvor.naziv;
                rtbOpis.Text = cvor.opis;
                var pic = Convert.FromBase64String(cvor.slika);
                
                using (MemoryStream ms = new MemoryStream(pic))
                {
                    pictureBoxSlika.Image = Image.FromStream(ms);
                }
            }
        }
        private void PrikaziKomsije()
        {
            int x = 10, y = 38,num=0;
            foreach(Stavka s in cvor.komsije)
            {
                Button button = new Button();
                button.Text = s.naziv;
                button.Left = x;
                button.Top = y;
                button.Click += buttonClick;
                Font font = new Font("Rockwell", 8);
                button.Font = font;
                button.Size = new Size(100, 25);
                button.BackColor = Color.DeepSkyBlue;
                groupBoxKomsije.Controls.Add(button);
                x += 100;
                num++;
                if (num == groupBoxKomsije.Size.Width/100-1)
                {
                    x = 10;
                    y += 25;
                }
            }
        }

        private void buttonClick(object sender,EventArgs e)
        {
            groupBoxKomsije.Controls.Clear();
            PronadjiCvor(((Button)sender).Text);
            PronadjiKomsije(((Button)sender).Text);
            PrikaziCvor();
            PrikaziKomsije();
        }

        private void FormEnKarta_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "hmteam");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            cvor = new Stavka();
            //komsije = new List<Stavka>();
        }

        private void buttonDodaj_Click(object sender, EventArgs e)
        {
            FormDodavanje f = new FormDodavanje();
            f.client = client;
            f.ShowDialog();
        }
    }
}
