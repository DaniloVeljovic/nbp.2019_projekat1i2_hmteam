﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    public class Stavka
    {
        public String naziv { get; set; }
        public String opis { get; set; }
        public String slika { get; set; }

        public List<Stavka> komsije { get; set; }

        public Stavka()
        {
            komsije = new List<Stavka>();
        }
    }
}
