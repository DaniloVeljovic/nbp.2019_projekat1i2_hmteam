﻿namespace Neo4J_Repository
{
    partial class FormEnKarta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxPretraga = new System.Windows.Forms.GroupBox();
            this.buttonDodaj = new System.Windows.Forms.Button();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxPrikaz = new System.Windows.Forms.GroupBox();
            this.groupBoxKomsije = new System.Windows.Forms.GroupBox();
            this.rtbOpis = new System.Windows.Forms.RichTextBox();
            this.pictureBoxSlika = new System.Windows.Forms.PictureBox();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.groupBoxPretraga.SuspendLayout();
            this.groupBoxPrikaz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlika)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxPretraga
            // 
            this.groupBoxPretraga.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxPretraga.BackColor = System.Drawing.Color.Silver;
            this.groupBoxPretraga.Controls.Add(this.buttonDodaj);
            this.groupBoxPretraga.Controls.Add(this.btnPretraga);
            this.groupBoxPretraga.Controls.Add(this.tbNaziv);
            this.groupBoxPretraga.Controls.Add(this.label1);
            this.groupBoxPretraga.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxPretraga.Location = new System.Drawing.Point(55, 46);
            this.groupBoxPretraga.Name = "groupBoxPretraga";
            this.groupBoxPretraga.Size = new System.Drawing.Size(1366, 115);
            this.groupBoxPretraga.TabIndex = 0;
            this.groupBoxPretraga.TabStop = false;
            this.groupBoxPretraga.Text = "Pretraga i dodavanje";
            // 
            // buttonDodaj
            // 
            this.buttonDodaj.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonDodaj.Location = new System.Drawing.Point(1083, 41);
            this.buttonDodaj.Name = "buttonDodaj";
            this.buttonDodaj.Size = new System.Drawing.Size(250, 42);
            this.buttonDodaj.TabIndex = 3;
            this.buttonDodaj.Text = "Dodavanje";
            this.buttonDodaj.UseVisualStyleBackColor = false;
            this.buttonDodaj.Click += new System.EventHandler(this.buttonDodaj_Click);
            // 
            // btnPretraga
            // 
            this.btnPretraga.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnPretraga.Location = new System.Drawing.Point(809, 41);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(250, 42);
            this.btnPretraga.TabIndex = 2;
            this.btnPretraga.Text = "Pretraga";
            this.btnPretraga.UseVisualStyleBackColor = false;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // tbNaziv
            // 
            this.tbNaziv.Location = new System.Drawing.Point(361, 48);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(334, 27);
            this.tbNaziv.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naziv pojma:";
            // 
            // groupBoxPrikaz
            // 
            this.groupBoxPrikaz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxPrikaz.BackColor = System.Drawing.Color.Silver;
            this.groupBoxPrikaz.Controls.Add(this.groupBoxKomsije);
            this.groupBoxPrikaz.Controls.Add(this.rtbOpis);
            this.groupBoxPrikaz.Controls.Add(this.pictureBoxSlika);
            this.groupBoxPrikaz.Controls.Add(this.lblNaziv);
            this.groupBoxPrikaz.Font = new System.Drawing.Font("Rockwell", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxPrikaz.Location = new System.Drawing.Point(55, 203);
            this.groupBoxPrikaz.Name = "groupBoxPrikaz";
            this.groupBoxPrikaz.Size = new System.Drawing.Size(1366, 581);
            this.groupBoxPrikaz.TabIndex = 1;
            this.groupBoxPrikaz.TabStop = false;
            this.groupBoxPrikaz.Text = "Prikaz";
            // 
            // groupBoxKomsije
            // 
            this.groupBoxKomsije.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxKomsije.BackColor = System.Drawing.Color.DarkGray;
            this.groupBoxKomsije.Location = new System.Drawing.Point(85, 425);
            this.groupBoxKomsije.Name = "groupBoxKomsije";
            this.groupBoxKomsije.Size = new System.Drawing.Size(1218, 130);
            this.groupBoxKomsije.TabIndex = 4;
            this.groupBoxKomsije.TabStop = false;
            this.groupBoxKomsije.Text = "Pojmovi";
            // 
            // rtbOpis
            // 
            this.rtbOpis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbOpis.Font = new System.Drawing.Font("Rockwell", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbOpis.Location = new System.Drawing.Point(85, 102);
            this.rtbOpis.Name = "rtbOpis";
            this.rtbOpis.ReadOnly = true;
            this.rtbOpis.Size = new System.Drawing.Size(610, 250);
            this.rtbOpis.TabIndex = 3;
            this.rtbOpis.Text = "";
            // 
            // pictureBoxSlika
            // 
            this.pictureBoxSlika.Location = new System.Drawing.Point(809, 102);
            this.pictureBoxSlika.Name = "pictureBoxSlika";
            this.pictureBoxSlika.Size = new System.Drawing.Size(250, 250);
            this.pictureBoxSlika.TabIndex = 2;
            this.pictureBoxSlika.TabStop = false;
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Rockwell", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.Location = new System.Drawing.Point(356, 49);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(158, 30);
            this.lblNaziv.TabIndex = 0;
            this.lblNaziv.Text = "Naziv pojma";
            // 
            // FormEnKarta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(1464, 819);
            this.Controls.Add(this.groupBoxPrikaz);
            this.Controls.Add(this.groupBoxPretraga);
            this.Name = "FormEnKarta";
            this.Text = "EnKarta";
            this.Load += new System.EventHandler(this.FormEnKarta_Load);
            this.groupBoxPretraga.ResumeLayout(false);
            this.groupBoxPretraga.PerformLayout();
            this.groupBoxPrikaz.ResumeLayout(false);
            this.groupBoxPrikaz.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlika)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxPretraga;
        private System.Windows.Forms.Button btnPretraga;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxPrikaz;
        private System.Windows.Forms.RichTextBox rtbOpis;
        private System.Windows.Forms.PictureBox pictureBoxSlika;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.GroupBox groupBoxKomsije;
        private System.Windows.Forms.Button buttonDodaj;
    }
}