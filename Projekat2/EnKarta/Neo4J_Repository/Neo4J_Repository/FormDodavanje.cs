﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Neo4jClient.Cypher;
using Neo4jClient;
using Neo4J_Repository.DomainModel;

namespace Neo4J_Repository
{
    public partial class FormDodavanje : Form
    {
        public GraphClient client;
        public FormDodavanje()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(openFileDialogSlika.ShowDialog()==DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialogSlika.FileName);
            }

        }

        private void btnDodajPojam_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("naziv",textBoxNaziv.Text);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Karta) and exists(n.naziv) and n.naziv =~ {naziv} return n",
                                                            queryDict, CypherResultMode.Set);

            List<Stavka> stavke = ((IRawGraphClient)client).ExecuteGetCypherResults<Stavka>(query).ToList();

            if (stavke.Count != 0)
            {
                MessageBox.Show("Enciklopedija sadrzi trazeni pojam!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (textBoxNaziv.Text.Length == 0 || textBoxOpis.Text.Length == 0 || pictureBox1.Image == null)
                {
                    MessageBox.Show("Sva polja moraju biti popunjena!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Dictionary<string, object> createDict = new Dictionary<string, object>();
                    createDict.Add("naziv", textBoxNaziv.Text);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                        createDict.Add("slika", Convert.ToBase64String(ms.ToArray()));
                    }
                    createDict.Add("opis", textBoxOpis.Text);
                    var newStavka = new Stavka { naziv = textBoxNaziv.Text, opis = textBoxOpis.Text, slika = createDict["slika"].ToString() };
                    client.Cypher.Create("(n:Karta {newStavka})").WithParam("newStavka", newStavka).ExecuteWithoutResults();
                    MessageBox.Show("Pojam je unet!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void UcitajPojmove()
        {
            List<Stavka> stavke = new List<Stavka>();
            stavke = client.Cypher.Match("(n:Karta)").Return<Stavka>(n => n.As<Stavka>()).Results.ToList();
            foreach(Stavka stavka in stavke)
            {
                comboBoxLevi.Items.Add(stavka.naziv);
                comboBoxDesni.Items.Add(stavka.naziv);
            }
        }

        private void FormDodavanje_Load(object sender, EventArgs e)
        {
            UcitajPojmove();
        }

        private void btnDodajVezu_Click(object sender, EventArgs e)
        {
            string leviPojam, desniPojam;
            leviPojam = comboBoxLevi.SelectedItem.ToString();
            desniPojam = comboBoxDesni.SelectedItem.ToString();
            client.Cypher
                .Match("(levi:Karta)", "(desni:Karta)")
                .Where((Stavka levi) => levi.naziv == leviPojam)
                .AndWhere((Stavka desni) => desni.naziv == desniPojam)
                .Create("(levi)-[:sadrzi]->(desni)")
                .ExecuteWithoutResults();
            MessageBox.Show("Veza je uneta!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
