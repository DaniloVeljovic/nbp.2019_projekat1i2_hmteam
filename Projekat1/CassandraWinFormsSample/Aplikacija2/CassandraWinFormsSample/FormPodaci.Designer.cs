﻿namespace CassandraWinFormsSample
{
    partial class FormPodaci
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbLokacija = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBrzinaVetra = new System.Windows.Forms.TextBox();
            this.tbKisa = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSneg = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbOblacnost = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTemperatura = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbUVIndeks = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbVazdusniPritisak = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbVlaznostVazduha = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbBrojMerenja = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.maxT = new System.Windows.Forms.TextBox();
            this.minT = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 565);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 179);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 51);
            this.button1.TabIndex = 1;
            this.button1.Text = "Zatvori";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CassandraWinFormsSample.Properties.Resources.final;
            this.pictureBox1.Location = new System.Drawing.Point(46, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(235, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Lokacija:";
            // 
            // tbLokacija
            // 
            this.tbLokacija.Location = new System.Drawing.Point(239, 52);
            this.tbLokacija.Name = "tbLokacija";
            this.tbLokacija.ReadOnly = true;
            this.tbLokacija.Size = new System.Drawing.Size(100, 32);
            this.tbLokacija.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(412, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Brzina vetra:";
            // 
            // tbBrzinaVetra
            // 
            this.tbBrzinaVetra.Location = new System.Drawing.Point(416, 52);
            this.tbBrzinaVetra.Name = "tbBrzinaVetra";
            this.tbBrzinaVetra.ReadOnly = true;
            this.tbBrzinaVetra.Size = new System.Drawing.Size(100, 32);
            this.tbBrzinaVetra.TabIndex = 4;
            // 
            // tbKisa
            // 
            this.tbKisa.Location = new System.Drawing.Point(601, 52);
            this.tbKisa.Name = "tbKisa";
            this.tbKisa.ReadOnly = true;
            this.tbKisa.Size = new System.Drawing.Size(100, 32);
            this.tbKisa.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(597, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kisa:";
            // 
            // tbSneg
            // 
            this.tbSneg.Location = new System.Drawing.Point(237, 157);
            this.tbSneg.Name = "tbSneg";
            this.tbSneg.ReadOnly = true;
            this.tbSneg.Size = new System.Drawing.Size(100, 32);
            this.tbSneg.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(233, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Sneg:";
            // 
            // tbOblacnost
            // 
            this.tbOblacnost.Location = new System.Drawing.Point(416, 157);
            this.tbOblacnost.Name = "tbOblacnost";
            this.tbOblacnost.ReadOnly = true;
            this.tbOblacnost.Size = new System.Drawing.Size(100, 32);
            this.tbOblacnost.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(412, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "Oblacnost:";
            // 
            // tbTemperatura
            // 
            this.tbTemperatura.Location = new System.Drawing.Point(601, 157);
            this.tbTemperatura.Name = "tbTemperatura";
            this.tbTemperatura.ReadOnly = true;
            this.tbTemperatura.Size = new System.Drawing.Size(100, 32);
            this.tbTemperatura.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(597, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 23);
            this.label6.TabIndex = 11;
            this.label6.Text = "Temperatura:";
            // 
            // tbUVIndeks
            // 
            this.tbUVIndeks.Location = new System.Drawing.Point(239, 274);
            this.tbUVIndeks.Name = "tbUVIndeks";
            this.tbUVIndeks.ReadOnly = true;
            this.tbUVIndeks.Size = new System.Drawing.Size(100, 32);
            this.tbUVIndeks.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(235, 250);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 23);
            this.label7.TabIndex = 13;
            this.label7.Text = "UV indeks:";
            // 
            // tbVazdusniPritisak
            // 
            this.tbVazdusniPritisak.Location = new System.Drawing.Point(394, 274);
            this.tbVazdusniPritisak.Name = "tbVazdusniPritisak";
            this.tbVazdusniPritisak.ReadOnly = true;
            this.tbVazdusniPritisak.Size = new System.Drawing.Size(137, 32);
            this.tbVazdusniPritisak.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(390, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(173, 23);
            this.label8.TabIndex = 15;
            this.label8.Text = "Vazdusni pritisak:";
            // 
            // tbVlaznostVazduha
            // 
            this.tbVlaznostVazduha.Location = new System.Drawing.Point(601, 274);
            this.tbVlaznostVazduha.Name = "tbVlaznostVazduha";
            this.tbVlaznostVazduha.ReadOnly = true;
            this.tbVlaznostVazduha.Size = new System.Drawing.Size(149, 32);
            this.tbVlaznostVazduha.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(597, 250);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(192, 23);
            this.label9.TabIndex = 17;
            this.label9.Text = "Vlaznost vazduha:";
            // 
            // tbBrojMerenja
            // 
            this.tbBrojMerenja.Location = new System.Drawing.Point(239, 367);
            this.tbBrojMerenja.Name = "tbBrojMerenja";
            this.tbBrojMerenja.ReadOnly = true;
            this.tbBrojMerenja.Size = new System.Drawing.Size(100, 32);
            this.tbBrojMerenja.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(235, 343);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 23);
            this.label10.TabIndex = 19;
            this.label10.Text = "Broj merenja:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.maxT);
            this.groupBox1.Controls.Add(this.minT);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(239, 436);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 117);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ekstremne vrednosti temperature";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(491, 64);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 23);
            this.label21.TabIndex = 62;
            this.label21.Text = "°C";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(197, 64);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 23);
            this.label20.TabIndex = 61;
            this.label20.Text = "°C";
            // 
            // maxT
            // 
            this.maxT.Location = new System.Drawing.Point(310, 61);
            this.maxT.Name = "maxT";
            this.maxT.ReadOnly = true;
            this.maxT.Size = new System.Drawing.Size(176, 32);
            this.maxT.TabIndex = 23;
            // 
            // minT
            // 
            this.minT.Location = new System.Drawing.Point(28, 61);
            this.minT.Name = "minT";
            this.minT.ReadOnly = true;
            this.minT.Size = new System.Drawing.Size(164, 32);
            this.minT.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(306, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(223, 23);
            this.label12.TabIndex = 22;
            this.label12.Text = "Maksimalna vrednost:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(206, 23);
            this.label11.TabIndex = 22;
            this.label11.Text = "Minimalna vrednost:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(521, 52);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 23);
            this.label13.TabIndex = 53;
            this.label13.Text = "m/s";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(521, 157);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 23);
            this.label14.TabIndex = 55;
            this.label14.Text = "%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(706, 157);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 23);
            this.label15.TabIndex = 56;
            this.label15.Text = "°C";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(342, 160);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 23);
            this.label16.TabIndex = 57;
            this.label16.Text = "cm";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(706, 55);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 23);
            this.label17.TabIndex = 58;
            this.label17.Text = "mm/m²";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(532, 277);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 23);
            this.label18.TabIndex = 59;
            this.label18.Text = "mbar";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(755, 277);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 23);
            this.label19.TabIndex = 60;
            this.label19.Text = "%";
            // 
            // FormPodaci
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 565);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbBrojMerenja);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbVlaznostVazduha);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbVazdusniPritisak);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbUVIndeks);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbTemperatura);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbOblacnost);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbSneg);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbKisa);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbBrzinaVetra);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbLokacija);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPodaci";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormPodaci";
            this.Load += new System.EventHandler(this.FormPodaci_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbLokacija;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBrzinaVetra;
        private System.Windows.Forms.TextBox tbKisa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbSneg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbOblacnost;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbTemperatura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbUVIndeks;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbVazdusniPritisak;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbVlaznostVazduha;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbBrojMerenja;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox maxT;
        private System.Windows.Forms.TextBox minT;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
    }
}