﻿namespace CassandraWinFormsSample
{
    partial class FormKorisnicka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnPrikaziVP = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpKrajVP = new System.Windows.Forms.DateTimePicker();
            this.dtpStartVP = new System.Windows.Forms.DateTimePicker();
            this.tbGradVP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrikaziZaDatum = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxJedanPeriod = new System.Windows.Forms.TextBox();
            this.dtpJedanPeriod = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbGradC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpKrajVP);
            this.groupBox1.Controls.Add(this.dtpStartVP);
            this.groupBox1.Controls.Add(this.tbGradVP);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(206, 258);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(709, 147);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vremeski period";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel3.Controls.Add(this.btnPrikaziVP);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(2, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(106, 118);
            this.panel3.TabIndex = 8;
            // 
            // btnPrikaziVP
            // 
            this.btnPrikaziVP.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnPrikaziVP.FlatAppearance.BorderSize = 0;
            this.btnPrikaziVP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikaziVP.ForeColor = System.Drawing.Color.White;
            this.btnPrikaziVP.Location = new System.Drawing.Point(0, 45);
            this.btnPrikaziVP.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrikaziVP.Name = "btnPrikaziVP";
            this.btnPrikaziVP.Size = new System.Drawing.Size(107, 32);
            this.btnPrikaziVP.TabIndex = 6;
            this.btnPrikaziVP.Text = "Prikazi";
            this.btnPrikaziVP.UseVisualStyleBackColor = false;
            this.btnPrikaziVP.Click += new System.EventHandler(this.btnPrikaziVP_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(425, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kraj perioda:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Pocetak perioda:";
            // 
            // dtpKrajVP
            // 
            this.dtpKrajVP.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpKrajVP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpKrajVP.Location = new System.Drawing.Point(427, 83);
            this.dtpKrajVP.Margin = new System.Windows.Forms.Padding(2);
            this.dtpKrajVP.Name = "dtpKrajVP";
            this.dtpKrajVP.Size = new System.Drawing.Size(151, 32);
            this.dtpKrajVP.TabIndex = 3;
            // 
            // dtpStartVP
            // 
            this.dtpStartVP.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpStartVP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartVP.Location = new System.Drawing.Point(249, 83);
            this.dtpStartVP.Margin = new System.Windows.Forms.Padding(2);
            this.dtpStartVP.Name = "dtpStartVP";
            this.dtpStartVP.Size = new System.Drawing.Size(151, 32);
            this.dtpStartVP.TabIndex = 2;
            // 
            // tbGradVP
            // 
            this.tbGradVP.Location = new System.Drawing.Point(137, 83);
            this.tbGradVP.Margin = new System.Windows.Forms.Padding(2);
            this.tbGradVP.Name = "tbGradVP";
            this.tbGradVP.Size = new System.Drawing.Size(76, 32);
            this.tbGradVP.TabIndex = 1;
            this.tbGradVP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbGradVP_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Grad:";
            // 
            // btnPrikaziZaDatum
            // 
            this.btnPrikaziZaDatum.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnPrikaziZaDatum.FlatAppearance.BorderSize = 0;
            this.btnPrikaziZaDatum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikaziZaDatum.ForeColor = System.Drawing.Color.White;
            this.btnPrikaziZaDatum.Location = new System.Drawing.Point(0, 36);
            this.btnPrikaziZaDatum.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrikaziZaDatum.Name = "btnPrikaziZaDatum";
            this.btnPrikaziZaDatum.Size = new System.Drawing.Size(106, 28);
            this.btnPrikaziZaDatum.TabIndex = 1;
            this.btnPrikaziZaDatum.Text = "Prikazi podatak";
            this.btnPrikaziZaDatum.UseVisualStyleBackColor = false;
            this.btnPrikaziZaDatum.Click += new System.EventHandler(this.btnPrikaziZaDatum_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(135, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Grad:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(271, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Datum:";
            // 
            // textBoxJedanPeriod
            // 
            this.textBoxJedanPeriod.Location = new System.Drawing.Point(137, 74);
            this.textBoxJedanPeriod.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxJedanPeriod.Name = "textBoxJedanPeriod";
            this.textBoxJedanPeriod.Size = new System.Drawing.Size(76, 32);
            this.textBoxJedanPeriod.TabIndex = 5;
            // 
            // dtpJedanPeriod
            // 
            this.dtpJedanPeriod.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpJedanPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpJedanPeriod.Location = new System.Drawing.Point(266, 74);
            this.dtpJedanPeriod.Margin = new System.Windows.Forms.Padding(2);
            this.dtpJedanPeriod.Name = "dtpJedanPeriod";
            this.dtpJedanPeriod.Size = new System.Drawing.Size(151, 32);
            this.dtpJedanPeriod.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 605);
            this.panel1.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CassandraWinFormsSample.Properties.Resources.final;
            this.pictureBox1.Location = new System.Drawing.Point(52, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 174);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 54);
            this.button1.TabIndex = 8;
            this.button1.Text = "Zatvori";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBoxJedanPeriod);
            this.groupBox2.Controls.Add(this.dtpJedanPeriod);
            this.groupBox2.Location = new System.Drawing.Point(206, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(708, 122);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pregledaj";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel2.Controls.Add(this.btnPrikaziZaDatum);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(3, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(106, 91);
            this.panel2.TabIndex = 7;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbGradC);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.panel4);
            this.groupBox3.Location = new System.Drawing.Point(209, 449);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(705, 127);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Prosecne vrednosti";
            // 
            // tbGradC
            // 
            this.tbGradC.Location = new System.Drawing.Point(204, 70);
            this.tbGradC.Margin = new System.Windows.Forms.Padding(2);
            this.tbGradC.Name = "tbGradC";
            this.tbGradC.Size = new System.Drawing.Size(76, 32);
            this.tbGradC.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 33);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Grad:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel4.Controls.Add(this.button2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(3, 28);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(103, 96);
            this.panel4.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(0, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 33);
            this.button2.TabIndex = 0;
            this.button2.Text = "Prikazi";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // FormKorisnicka
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(926, 605);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormKorisnicka";
            this.Text = "u";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpKrajVP;
        private System.Windows.Forms.DateTimePicker dtpStartVP;
        private System.Windows.Forms.TextBox tbGradVP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrikaziVP;
        private System.Windows.Forms.Button btnPrikaziZaDatum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxJedanPeriod;
        private System.Windows.Forms.DateTimePicker dtpJedanPeriod;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbGradC;
        private System.Windows.Forms.Label label6;
    }
}