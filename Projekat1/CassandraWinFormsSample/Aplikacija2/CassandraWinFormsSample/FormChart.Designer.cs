﻿namespace CassandraWinFormsSample
{
    partial class FormChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartBrzinaVetra = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartKisa = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartOblacnost = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartSneg = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartTemperatura = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartUVIndeks = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartVazdusniPritisak = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartVlaznostVazduha = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelBrzinaVetra = new System.Windows.Forms.Label();
            this.labelKisa = new System.Windows.Forms.Label();
            this.labelOblacnost = new System.Windows.Forms.Label();
            this.labelSneg = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelTemperatura = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelUVIndeks = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelVazdusniPritisak = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelVlaznostVazduha = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelPravacVetra = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartBrzinaVetra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartKisa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOblacnost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSneg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTemperatura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartUVIndeks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartVazdusniPritisak)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartVlaznostVazduha)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // chartBrzinaVetra
            // 
            chartArea1.Name = "ChartArea1";
            this.chartBrzinaVetra.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartBrzinaVetra.Legends.Add(legend1);
            this.chartBrzinaVetra.Location = new System.Drawing.Point(145, 11);
            this.chartBrzinaVetra.Margin = new System.Windows.Forms.Padding(2);
            this.chartBrzinaVetra.Name = "chartBrzinaVetra";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartBrzinaVetra.Series.Add(series1);
            this.chartBrzinaVetra.Size = new System.Drawing.Size(360, 175);
            this.chartBrzinaVetra.TabIndex = 0;
            this.chartBrzinaVetra.Text = "chartBrzinaVetra";
            // 
            // chartKisa
            // 
            chartArea2.Name = "ChartArea1";
            this.chartKisa.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartKisa.Legends.Add(legend2);
            this.chartKisa.Location = new System.Drawing.Point(608, 11);
            this.chartKisa.Margin = new System.Windows.Forms.Padding(2);
            this.chartKisa.Name = "chartKisa";
            this.chartKisa.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartKisa.Series.Add(series2);
            this.chartKisa.Size = new System.Drawing.Size(360, 175);
            this.chartKisa.TabIndex = 1;
            this.chartKisa.Text = "chartKisa";
            // 
            // chartOblacnost
            // 
            chartArea3.Name = "ChartArea1";
            this.chartOblacnost.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartOblacnost.Legends.Add(legend3);
            this.chartOblacnost.Location = new System.Drawing.Point(1047, 11);
            this.chartOblacnost.Margin = new System.Windows.Forms.Padding(2);
            this.chartOblacnost.Name = "chartOblacnost";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chartOblacnost.Series.Add(series3);
            this.chartOblacnost.Size = new System.Drawing.Size(328, 175);
            this.chartOblacnost.TabIndex = 2;
            this.chartOblacnost.Text = "chartOblacnost";
            // 
            // chartSneg
            // 
            chartArea4.Name = "ChartArea1";
            this.chartSneg.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chartSneg.Legends.Add(legend4);
            this.chartSneg.Location = new System.Drawing.Point(145, 293);
            this.chartSneg.Margin = new System.Windows.Forms.Padding(2);
            this.chartSneg.Name = "chartSneg";
            this.chartSneg.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chartSneg.Series.Add(series4);
            this.chartSneg.Size = new System.Drawing.Size(360, 175);
            this.chartSneg.TabIndex = 4;
            this.chartSneg.Text = "chartSneg";
            // 
            // chartTemperatura
            // 
            chartArea5.Name = "ChartArea1";
            this.chartTemperatura.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chartTemperatura.Legends.Add(legend5);
            this.chartTemperatura.Location = new System.Drawing.Point(1046, 293);
            this.chartTemperatura.Margin = new System.Windows.Forms.Padding(2);
            this.chartTemperatura.Name = "chartTemperatura";
            this.chartTemperatura.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.chartTemperatura.Series.Add(series5);
            this.chartTemperatura.Size = new System.Drawing.Size(328, 175);
            this.chartTemperatura.TabIndex = 5;
            this.chartTemperatura.Text = "chartTemperatura";
            // 
            // chartUVIndeks
            // 
            chartArea6.Name = "ChartArea1";
            this.chartUVIndeks.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chartUVIndeks.Legends.Add(legend6);
            this.chartUVIndeks.Location = new System.Drawing.Point(145, 564);
            this.chartUVIndeks.Margin = new System.Windows.Forms.Padding(2);
            this.chartUVIndeks.Name = "chartUVIndeks";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.chartUVIndeks.Series.Add(series6);
            this.chartUVIndeks.Size = new System.Drawing.Size(360, 175);
            this.chartUVIndeks.TabIndex = 6;
            this.chartUVIndeks.Text = "chartUVIndeks";
            // 
            // chartVazdusniPritisak
            // 
            chartArea7.Name = "ChartArea1";
            this.chartVazdusniPritisak.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.chartVazdusniPritisak.Legends.Add(legend7);
            this.chartVazdusniPritisak.Location = new System.Drawing.Point(618, 564);
            this.chartVazdusniPritisak.Margin = new System.Windows.Forms.Padding(2);
            this.chartVazdusniPritisak.Name = "chartVazdusniPritisak";
            this.chartVazdusniPritisak.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series7.ChartArea = "ChartArea1";
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            this.chartVazdusniPritisak.Series.Add(series7);
            this.chartVazdusniPritisak.Size = new System.Drawing.Size(360, 175);
            this.chartVazdusniPritisak.TabIndex = 7;
            this.chartVazdusniPritisak.Text = "chartVazdusniPritisak";
            // 
            // chartVlaznostVazduha
            // 
            chartArea8.Name = "ChartArea1";
            this.chartVlaznostVazduha.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.chartVlaznostVazduha.Legends.Add(legend8);
            this.chartVlaznostVazduha.Location = new System.Drawing.Point(1047, 564);
            this.chartVlaznostVazduha.Margin = new System.Windows.Forms.Padding(2);
            this.chartVlaznostVazduha.Name = "chartVlaznostVazduha";
            series8.ChartArea = "ChartArea1";
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            this.chartVlaznostVazduha.Series.Add(series8);
            this.chartVlaznostVazduha.Size = new System.Drawing.Size(329, 175);
            this.chartVlaznostVazduha.TabIndex = 8;
            this.chartVlaznostVazduha.Text = "chartVlaznostVazduha";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 200);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Prosecna vrednost:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(647, 200);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 21);
            this.label2.TabIndex = 10;
            this.label2.Text = "Prosecna vrednost:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1155, 200);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 21);
            this.label3.TabIndex = 11;
            this.label3.Text = "Prosecna vrednost:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // labelBrzinaVetra
            // 
            this.labelBrzinaVetra.AutoSize = true;
            this.labelBrzinaVetra.Location = new System.Drawing.Point(307, 200);
            this.labelBrzinaVetra.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelBrzinaVetra.Name = "labelBrzinaVetra";
            this.labelBrzinaVetra.Size = new System.Drawing.Size(60, 21);
            this.labelBrzinaVetra.TabIndex = 18;
            this.labelBrzinaVetra.Text = "label4";
            // 
            // labelKisa
            // 
            this.labelKisa.AutoSize = true;
            this.labelKisa.Location = new System.Drawing.Point(837, 200);
            this.labelKisa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelKisa.Name = "labelKisa";
            this.labelKisa.Size = new System.Drawing.Size(69, 21);
            this.labelKisa.TabIndex = 19;
            this.labelKisa.Text = "label10";
            // 
            // labelOblacnost
            // 
            this.labelOblacnost.AutoSize = true;
            this.labelOblacnost.Location = new System.Drawing.Point(1305, 200);
            this.labelOblacnost.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOblacnost.Name = "labelOblacnost";
            this.labelOblacnost.Size = new System.Drawing.Size(69, 21);
            this.labelOblacnost.TabIndex = 20;
            this.labelOblacnost.Text = "label11";
            // 
            // labelSneg
            // 
            this.labelSneg.AutoSize = true;
            this.labelSneg.Location = new System.Drawing.Point(307, 495);
            this.labelSneg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSneg.Name = "labelSneg";
            this.labelSneg.Size = new System.Drawing.Size(69, 21);
            this.labelSneg.TabIndex = 22;
            this.labelSneg.Text = "label11";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(145, 495);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(171, 21);
            this.label5.TabIndex = 21;
            this.label5.Text = "Prosecna vrednost:";
            // 
            // labelTemperatura
            // 
            this.labelTemperatura.AutoSize = true;
            this.labelTemperatura.Location = new System.Drawing.Point(1290, 495);
            this.labelTemperatura.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTemperatura.Name = "labelTemperatura";
            this.labelTemperatura.Size = new System.Drawing.Size(69, 21);
            this.labelTemperatura.TabIndex = 24;
            this.labelTemperatura.Text = "label11";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1155, 495);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 21);
            this.label6.TabIndex = 23;
            this.label6.Text = "Prosecna vrednost:";
            // 
            // labelUVIndeks
            // 
            this.labelUVIndeks.AutoSize = true;
            this.labelUVIndeks.Location = new System.Drawing.Point(307, 754);
            this.labelUVIndeks.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelUVIndeks.Name = "labelUVIndeks";
            this.labelUVIndeks.Size = new System.Drawing.Size(69, 21);
            this.labelUVIndeks.TabIndex = 26;
            this.labelUVIndeks.Text = "label11";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(142, 754);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(171, 21);
            this.label10.TabIndex = 25;
            this.label10.Text = "Prosecna vrednost:";
            // 
            // labelVazdusniPritisak
            // 
            this.labelVazdusniPritisak.AutoSize = true;
            this.labelVazdusniPritisak.Location = new System.Drawing.Point(852, 754);
            this.labelVazdusniPritisak.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelVazdusniPritisak.Name = "labelVazdusniPritisak";
            this.labelVazdusniPritisak.Size = new System.Drawing.Size(69, 21);
            this.labelVazdusniPritisak.TabIndex = 28;
            this.labelVazdusniPritisak.Text = "label11";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(683, 754);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 21);
            this.label7.TabIndex = 27;
            this.label7.Text = "Prosecna vrednost:";
            // 
            // labelVlaznostVazduha
            // 
            this.labelVlaznostVazduha.AutoSize = true;
            this.labelVlaznostVazduha.Location = new System.Drawing.Point(1305, 754);
            this.labelVlaznostVazduha.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelVlaznostVazduha.Name = "labelVlaznostVazduha";
            this.labelVlaznostVazduha.Size = new System.Drawing.Size(69, 21);
            this.labelVlaznostVazduha.TabIndex = 30;
            this.labelVlaznostVazduha.Text = "label11";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1153, 754);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(171, 21);
            this.label8.TabIndex = 29;
            this.label8.Text = "Prosecna vrednost:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(137, 788);
            this.panel1.TabIndex = 31;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 184);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 46);
            this.button1.TabIndex = 1;
            this.button1.Text = "Zatvori";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CassandraWinFormsSample.Properties.Resources.final;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // labelPravacVetra
            // 
            this.labelPravacVetra.AutoSize = true;
            this.labelPravacVetra.Location = new System.Drawing.Point(626, 367);
            this.labelPravacVetra.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPravacVetra.Name = "labelPravacVetra";
            this.labelPravacVetra.Size = new System.Drawing.Size(171, 21);
            this.labelPravacVetra.TabIndex = 32;
            this.labelPravacVetra.Text = "Prosecna vrednost:";
            // 
            // FormChart
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1386, 788);
            this.Controls.Add(this.labelPravacVetra);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelVlaznostVazduha);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelVazdusniPritisak);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelUVIndeks);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labelTemperatura);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelSneg);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelOblacnost);
            this.Controls.Add(this.labelKisa);
            this.Controls.Add(this.labelBrzinaVetra);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chartVlaznostVazduha);
            this.Controls.Add(this.chartVazdusniPritisak);
            this.Controls.Add(this.chartUVIndeks);
            this.Controls.Add(this.chartTemperatura);
            this.Controls.Add(this.chartSneg);
            this.Controls.Add(this.chartOblacnost);
            this.Controls.Add(this.chartKisa);
            this.Controls.Add(this.chartBrzinaVetra);
            this.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormChart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormChart";
            ((System.ComponentModel.ISupportInitialize)(this.chartBrzinaVetra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartKisa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOblacnost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSneg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTemperatura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartUVIndeks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartVazdusniPritisak)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartVlaznostVazduha)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartBrzinaVetra;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartKisa;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartOblacnost;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSneg;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTemperatura;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartUVIndeks;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartVazdusniPritisak;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartVlaznostVazduha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelBrzinaVetra;
        private System.Windows.Forms.Label labelKisa;
        private System.Windows.Forms.Label labelOblacnost;
        private System.Windows.Forms.Label labelSneg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelTemperatura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelUVIndeks;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelVazdusniPritisak;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelVlaznostVazduha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelPravacVetra;
    }
}