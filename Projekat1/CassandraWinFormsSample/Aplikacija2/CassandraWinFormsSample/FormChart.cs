﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace CassandraWinFormsSample
{
    public partial class FormChart : Form
    {
        

        public FormChart()
        {
            InitializeComponent();
        }

        public FormChart(List<Parametri> lista,Parametri prosek)
        {
            InitializeComponent();
            Postavi(lista,prosek);
            
        }

        public void Postavi(List<Parametri> lista,Parametri prosek)
        {
            chartBrzinaVetra.Series[0].Points.Clear();
            chartKisa.Series[0].Points.Clear();
            chartOblacnost.Series[0].Points.Clear();
            chartSneg.Series[0].Points.Clear();
            chartTemperatura.Series[0].Points.Clear();
            chartUVIndeks.Series[0].Points.Clear();
            chartVazdusniPritisak.Series[0].Points.Clear();
            chartVlaznostVazduha.Series[0].Points.Clear();

            chartBrzinaVetra.Series[0].Name = "Brzina vetra";
            chartKisa.Series[0].Name = "Kisa";
            chartOblacnost.Series[0].Name = "Oblacnost";
            chartSneg.Series[0].Name = "Sneg";
            chartTemperatura.Series[0].Name = "Temperatura";
            chartUVIndeks.Series[0].Name = "UV indeks";
            chartVazdusniPritisak.Series[0].Name = "Vazdusni pritisak";
            chartVlaznostVazduha.Series[0].Name = "Vlaznost vazduha";

            for (int i=0;i<lista.Count;i++)
            {
                
                chartBrzinaVetra.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString()+" "+lista[i].DatumIVreme.ToShortTimeString(), lista[i].BrzinaVetra);
                chartKisa.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].Kisa);
                chartOblacnost.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].Oblacnost);
                chartSneg.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].Sneg);
                chartTemperatura.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].Temperatura);
                chartUVIndeks.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].UVIndeks);
                chartVazdusniPritisak.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].VazdusniPritisak);
                chartVlaznostVazduha.Series[0].Points.AddXY(lista[i].DatumIVreme.ToShortDateString() + " " + lista[i].DatumIVreme.ToShortTimeString(), lista[i].VlaznostVazduha);
            }

            labelBrzinaVetra.Text = prosek.BrzinaVetra.ToString()+" m/s";
            labelKisa.Text = prosek.Kisa.ToString() + " mm/m²";
            labelOblacnost.Text = prosek.Oblacnost.ToString() + " %";
            labelSneg.Text = prosek.Sneg.ToString() + " cm";
            labelTemperatura.Text = prosek.Temperatura.ToString() + " °C";
            labelUVIndeks.Text = prosek.UVIndeks.ToString();
            labelVazdusniPritisak.Text = prosek.VazdusniPritisak.ToString() + " mbar";
            labelVlaznostVazduha.Text = prosek.VlaznostVazduha.ToString() + " %";
            labelPravacVetra.Text = "Pravac vetra: "+prosek.PravacVetra;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
