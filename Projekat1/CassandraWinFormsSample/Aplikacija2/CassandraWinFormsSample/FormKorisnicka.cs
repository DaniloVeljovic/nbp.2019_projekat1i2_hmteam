﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace CassandraWinFormsSample
{
    public partial class FormKorisnicka : Form
    {
        public FormKorisnicka()
        {
            InitializeComponent();
            dtpKrajVP.Value=dtpStartVP.Value=dtpJedanPeriod.Value= DateTime.Parse("2000-05-20 00:00:00.000+0000");
        }

        private void tbGradVP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ' ')
            {
                e.Handled = true;
            }
        }

        private void btnPrikaziVP_Click(object sender, EventArgs e)
        {
            List<Parametri> lista = new List<Parametri>(); 
            lista = DataProvider.PribaviParametre(tbGradVP.Text, dtpStartVP.Value, dtpKrajVP.Value);
            Parametri prosek = new Parametri();
            prosek = DataProvider.ProsecnaVrednost(tbGradVP.Text, dtpStartVP.Value, dtpKrajVP.Value);
            Form f = new FormChart(lista,prosek);
            f.ShowDialog();
        }

        private void btnPrikaziZaDatum_Click(object sender, EventArgs e)
        {
            Parametri parametar = DataProvider.PribaviParametar(textBoxJedanPeriod.Text, dtpJedanPeriod.Value);

            if (parametar == null)
            {
                MessageBox.Show("Trazeni podatak ne postoji!", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

                Form f = new FormAzuriranje(parametar);
                f.ShowDialog();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TemperaturaEkstremi te = DataProvider.PribaviTempEkstrem(tbGradC.Text);
            ProsecneVrednosti pv = DataProvider.PribaviProsecnuVrednost(tbGradC.Text);

            Form f = new FormPodaci(pv, te);
            f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            TemperaturaEkstremi te = DataProvider.PribaviTempEkstrem(tbGradC.Text);
            ProsecneVrednosti pv = DataProvider.PribaviProsecnuVrednost(tbGradC.Text);

            if (te == null && pv == null)
            {
                MessageBox.Show("Trazeni podatak ne postoji!", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Form f = new FormPodaci(pv, te);
                f.ShowDialog();
            }
        }
    }
}
