﻿namespace CassandraWinFormsSample
{
    partial class FormAzuriranje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbVlaznostVazduha = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbVazdusniPritisak = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbUV = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbSneg = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbTemperatura = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbOblacnost = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbPravacVetra = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbKisa = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbBrzinaVetra = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDatumA = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tbGradA = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(441, 237);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 21);
            this.label18.TabIndex = 58;
            this.label18.Text = "mbar";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(630, 210);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 21);
            this.label17.TabIndex = 57;
            this.label17.Text = "%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(631, 139);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 21);
            this.label16.TabIndex = 56;
            this.label16.Text = "cm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(441, 181);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 21);
            this.label15.TabIndex = 55;
            this.label15.Text = "°C";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(293, 181);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 21);
            this.label14.TabIndex = 54;
            this.label14.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(631, 60);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 21);
            this.label13.TabIndex = 53;
            this.label13.Text = "mm/m²";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(293, 121);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 21);
            this.label12.TabIndex = 52;
            this.label12.Text = "m/s";
            // 
            // tbVlaznostVazduha
            // 
            this.tbVlaznostVazduha.Location = new System.Drawing.Point(548, 207);
            this.tbVlaznostVazduha.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbVlaznostVazduha.Name = "tbVlaznostVazduha";
            this.tbVlaznostVazduha.Size = new System.Drawing.Size(76, 27);
            this.tbVlaznostVazduha.TabIndex = 51;
            this.tbVlaznostVazduha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVlaznostVazduha_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(544, 181);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 21);
            this.label11.TabIndex = 50;
            this.label11.Text = "Vlaznost vazduha:";
            // 
            // tbVazdusniPritisak
            // 
            this.tbVazdusniPritisak.Location = new System.Drawing.Point(360, 234);
            this.tbVazdusniPritisak.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbVazdusniPritisak.Name = "tbVazdusniPritisak";
            this.tbVazdusniPritisak.Size = new System.Drawing.Size(76, 27);
            this.tbVazdusniPritisak.TabIndex = 49;
            this.tbVazdusniPritisak.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVazdusniPritisak_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(356, 211);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 21);
            this.label10.TabIndex = 48;
            this.label10.Text = "Vazdusni pritisak:";
            // 
            // tbUV
            // 
            this.tbUV.Location = new System.Drawing.Point(213, 234);
            this.tbUV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbUV.MaxLength = 5;
            this.tbUV.Name = "tbUV";
            this.tbUV.Size = new System.Drawing.Size(76, 27);
            this.tbUV.TabIndex = 47;
            this.tbUV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbUV_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(209, 211);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 21);
            this.label9.TabIndex = 46;
            this.label9.Text = "UV indeks:";
            // 
            // tbSneg
            // 
            this.tbSneg.Location = new System.Drawing.Point(548, 136);
            this.tbSneg.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbSneg.MaxLength = 7;
            this.tbSneg.Name = "tbSneg";
            this.tbSneg.Size = new System.Drawing.Size(76, 27);
            this.tbSneg.TabIndex = 45;
            this.tbSneg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSneg_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(545, 108);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 21);
            this.label8.TabIndex = 44;
            this.label8.Text = "Sneg:";
            // 
            // tbTemperatura
            // 
            this.tbTemperatura.Location = new System.Drawing.Point(360, 178);
            this.tbTemperatura.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbTemperatura.MaxLength = 5;
            this.tbTemperatura.Name = "tbTemperatura";
            this.tbTemperatura.Size = new System.Drawing.Size(76, 27);
            this.tbTemperatura.TabIndex = 43;
            this.tbTemperatura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTemperatura_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(356, 155);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 21);
            this.label7.TabIndex = 42;
            this.label7.Text = "Temperatura:";
            // 
            // tbOblacnost
            // 
            this.tbOblacnost.Location = new System.Drawing.Point(213, 178);
            this.tbOblacnost.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbOblacnost.MaxLength = 3;
            this.tbOblacnost.Name = "tbOblacnost";
            this.tbOblacnost.Size = new System.Drawing.Size(76, 27);
            this.tbOblacnost.TabIndex = 41;
            this.tbOblacnost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbOblacnost_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(209, 155);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 21);
            this.label6.TabIndex = 40;
            this.label6.Text = "Oblacnost:";
            // 
            // cbPravacVetra
            // 
            this.cbPravacVetra.FormattingEnabled = true;
            this.cbPravacVetra.Items.AddRange(new object[] {
            "sever",
            "jug",
            "istok",
            "zapad",
            "severozapad",
            "severoistok",
            "jugozapad",
            "jugoistok"});
            this.cbPravacVetra.Location = new System.Drawing.Point(360, 121);
            this.cbPravacVetra.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbPravacVetra.Name = "cbPravacVetra";
            this.cbPravacVetra.Size = new System.Drawing.Size(92, 29);
            this.cbPravacVetra.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(356, 95);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 21);
            this.label5.TabIndex = 38;
            this.label5.Text = "Pravac vetra:";
            // 
            // tbKisa
            // 
            this.tbKisa.Location = new System.Drawing.Point(548, 57);
            this.tbKisa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbKisa.MaxLength = 4;
            this.tbKisa.Name = "tbKisa";
            this.tbKisa.Size = new System.Drawing.Size(76, 27);
            this.tbKisa.TabIndex = 37;
            this.tbKisa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKisa_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(545, 30);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 21);
            this.label4.TabIndex = 36;
            this.label4.Text = "Kisa:";
            // 
            // tbBrzinaVetra
            // 
            this.tbBrzinaVetra.Location = new System.Drawing.Point(213, 118);
            this.tbBrzinaVetra.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBrzinaVetra.MaxLength = 5;
            this.tbBrzinaVetra.Name = "tbBrzinaVetra";
            this.tbBrzinaVetra.Size = new System.Drawing.Size(76, 27);
            this.tbBrzinaVetra.TabIndex = 35;
            this.tbBrzinaVetra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBrzinaVetra_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 21);
            this.label3.TabIndex = 34;
            this.label3.Text = "Brzina vetra:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(356, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 21);
            this.label2.TabIndex = 33;
            this.label2.Text = "Datum i vreme:";
            // 
            // dtpDatumA
            // 
            this.dtpDatumA.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpDatumA.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumA.Location = new System.Drawing.Point(360, 55);
            this.dtpDatumA.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpDatumA.Name = "dtpDatumA";
            this.dtpDatumA.Size = new System.Drawing.Size(110, 27);
            this.dtpDatumA.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(208, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 21);
            this.label1.TabIndex = 31;
            this.label1.Text = "Grad:";
            // 
            // tbGradA
            // 
            this.tbGradA.Location = new System.Drawing.Point(212, 55);
            this.tbGradA.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbGradA.Name = "tbGradA";
            this.tbGradA.ReadOnly = true;
            this.tbGradA.Size = new System.Drawing.Size(76, 27);
            this.tbGradA.TabIndex = 30;
            this.tbGradA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbGradA_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(145, 305);
            this.panel1.TabIndex = 59;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CassandraWinFormsSample.Properties.Resources.final;
            this.pictureBox1.Location = new System.Drawing.Point(22, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 60;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 46);
            this.button1.TabIndex = 60;
            this.button1.Text = "Zatvori";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormAzuriranje
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(724, 305);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbVlaznostVazduha);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbVazdusniPritisak);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbUV);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbSneg);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbTemperatura);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbOblacnost);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbPravacVetra);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbKisa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbBrzinaVetra);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpDatumA);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbGradA);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormAzuriranje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Azuriranje podataka";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbVlaznostVazduha;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbVazdusniPritisak;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbUV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbSneg;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbTemperatura;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbOblacnost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbPravacVetra;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbKisa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbBrzinaVetra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDatumA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbGradA;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
    }
}