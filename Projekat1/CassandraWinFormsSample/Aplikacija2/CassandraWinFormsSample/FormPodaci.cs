﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace CassandraWinFormsSample
{
    public partial class FormPodaci : Form
    {
        private ProsecneVrednosti p;
        private TemperaturaEkstremi t;
        public FormPodaci()
        {
            InitializeComponent();
        }

        public FormPodaci(ProsecneVrednosti p, TemperaturaEkstremi t)
        {
            InitializeComponent();
            this.p = p;
            this.t = t;
        }

        private void FormPodaci_Load(object sender, EventArgs e)
        {
            NapuniKomponente(p, t);
        }

        private void NapuniKomponente(ProsecneVrednosti p, TemperaturaEkstremi t)
        {
            tbLokacija.Text = p.Lokacija;
            tbBrzinaVetra.Text = p.BrzinaVetra.ToString();
            tbKisa.Text = p.Kisa.ToString();
            tbOblacnost.Text = p.Oblacnost.ToString();
            tbSneg.Text = p.Sneg.ToString();
            tbTemperatura.Text = p.Temperatura.ToString();
            tbUVIndeks.Text = p.UVIndeks.ToString();
            tbVazdusniPritisak.Text = p.VazdusniPritisak.ToString();
            tbVlaznostVazduha.Text = p.VlaznostVazduha.ToString();
            tbBrojMerenja.Text = p.BrojMerenja.ToString();
            minT.Text = t.MinTemperatura.ToString();
            maxT.Text = t.MaxTemperatura.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
