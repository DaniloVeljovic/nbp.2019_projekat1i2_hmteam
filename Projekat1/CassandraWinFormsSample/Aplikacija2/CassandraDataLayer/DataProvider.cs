﻿using Cassandra;
using CassandraDataLayer.QueryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CassandraDataLayer
{
    public static class DataProvider
    {
        #region Parametri

        public static void DodajParametar(Parametri p)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet parametarPodaci = session.Execute("insert into \"parametri\" (\"LOKACIJA\", \"DATUM_I_VREME\", \"BRZINA_VETRA\", \"KISA\", \"OBLACNOST\", \"PRAVAC_VETRA\", \"SNEG\", \"TEMPERATURA\", \"UV_INDEKS\",\"VAZDUSNI_PRITISAK\", \"VLAZNOST_VAZDUHA\")  values ('" + p.Lokacija + "', '" + p.DatumIVreme.ToString("yyyy-MM-dd hh:mm:ss.000+0000") + "', " + p.BrzinaVetra + ", " + p.Kisa + ", " + p.Oblacnost + ", '" + p.PravacVetra + "', " + p.Sneg + ", " + p.Temperatura + ", " + p.UVIndeks + "," + p.VazdusniPritisak + ", " + p.VlaznostVazduha + ")");

            DodajLokaciju(p.Lokacija);
            ProsecneVrednostiNoviParametar(p);
            TempEkstremiDodavanjeParametra(p);
        }

        public static Parametri PribaviParametar(string lokacija, DateTime datum)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            Row parametar = session.Execute("select * from \"parametri\" where \"LOKACIJA\"='" + lokacija + "' and \"DATUM_I_VREME\"='" + datum.ToString("yyyy-MM-dd hh:mm:ss.000+0000") + "'").FirstOrDefault();

            if (parametar != null)
            {
                //var parametar = parametarPodaci.ElementAt(0);
                Parametri p = new Parametri();
                p.Lokacija = parametar["LOKACIJA"] != null ? parametar["LOKACIJA"].ToString() : String.Empty;
                p.DatumIVreme = datum;
                p.BrzinaVetra = (float)parametar["BRZINA_VETRA"];
                p.Kisa = (int)parametar["KISA"];
                p.Oblacnost = (int)parametar["OBLACNOST"];
                p.PravacVetra = parametar["PRAVAC_VETRA"] != null ? parametar["PRAVAC_VETRA"].ToString() : String.Empty;
                p.Sneg = (float)parametar["SNEG"];
                p.Temperatura = (float)parametar["TEMPERATURA"];
                p.UVIndeks = (float)parametar["UV_INDEKS"];
                p.VazdusniPritisak = (float)parametar["VAZDUSNI_PRITISAK"];
                p.VlaznostVazduha = (float)parametar["VLAZNOST_VAZDUHA"];
                return p;
            }
            else
                return null;
        }

        private static List<Parametri> PribaviParametrePartitionKey(string lokacija)
        {
            ISession session = SessionManager.GetSession();
            List<Parametri> parametri = new List<Parametri>();

            if (session == null)
                return null;

            var parametarPodaci = session.Execute("select * from \"parametri\" where \"LOKACIJA\"='" + lokacija + "'");

            if (parametarPodaci != null)
            {
                foreach (var parametar in parametarPodaci)
                {
                    Parametri p = new Parametri();
                    p.Lokacija = parametar["LOKACIJA"] != null ? parametar["LOKACIJA"].ToString() : String.Empty;
                    p.BrzinaVetra = (float)parametar["BRZINA_VETRA"];
                    p.Kisa = (int)parametar["KISA"];
                    p.Oblacnost = (int)parametar["OBLACNOST"];
                    p.PravacVetra = parametar["PRAVAC_VETRA"] != null ? parametar["PRAVAC_VETRA"].ToString() : String.Empty;
                    p.Sneg = (float)parametar["SNEG"];
                    p.Temperatura = (float)parametar["TEMPERATURA"];
                    p.UVIndeks = (float)parametar["UV_INDEKS"];
                    p.VazdusniPritisak = (float)parametar["VAZDUSNI_PRITISAK"];
                    p.VlaznostVazduha = (float)parametar["VLAZNOST_VAZDUHA"];
                    parametri.Add(p);
                }

                return parametri;
            }
            else
                return null;
        }

        public static List<Parametri> PribaviParametre(string lokacija, DateTime start, DateTime kraj)
        {
            ISession session = SessionManager.GetSession();
            List<Parametri> parametri = new List<Parametri>();

            if (session == null)
                return null;
            DateTime pat = start;
            var parametarPodaci = session.Execute("select * from \"parametri\" where \"LOKACIJA\"='" + lokacija + "' and \"DATUM_I_VREME\">='" + start.ToString("yyyy-MM-dd hh:mm:ss.000+0000") + "' and \"DATUM_I_VREME\"<='" + kraj.ToString("yyyy-MM-dd hh:mm:ss.000+0000") + "'");

            if (parametarPodaci != null)
            {
                foreach (var parametar in parametarPodaci)
                {
                    Parametri p = new Parametri();
                    p.Lokacija = parametar["LOKACIJA"] != null ? parametar["LOKACIJA"].ToString() : String.Empty;
                    p.DatumIVreme = pat;
                    p.BrzinaVetra = (float)parametar["BRZINA_VETRA"];
                    p.Kisa = (int)parametar["KISA"];
                    p.Oblacnost = (int)parametar["OBLACNOST"];
                    p.PravacVetra = parametar["PRAVAC_VETRA"] != null ? parametar["PRAVAC_VETRA"].ToString() : String.Empty;
                    p.Sneg = (float)parametar["SNEG"];
                    p.Temperatura = (float)parametar["TEMPERATURA"];
                    p.UVIndeks = (float)parametar["UV_INDEKS"];
                    p.VazdusniPritisak = (float)parametar["VAZDUSNI_PRITISAK"];
                    p.VlaznostVazduha = (float)parametar["VLAZNOST_VAZDUHA"];
                    parametri.Add(p);
                    pat = pat.AddHours(1);
                }

                return parametri;
            }
            else
                return null;
        }

        public static void ObrisiParametar(string lokacija, DateTime datum)
        {
            Parametri p = PribaviParametar(lokacija, datum);


            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet parametri = session.Execute("delete from \"parametri\" where \"LOKACIJA\" = '" + lokacija + "' and \"DATUM_I_VREME\" = '" + datum.ToString("yyyy-MM-dd hh:mm:ss.000+0000") + "'");

            ObrisiLokacije(lokacija);
            ProsecneVrednostiBrisanjeParametra(p);
            TempEkstremiBrisanjeParametra(p);
        }

        public static void AzurirajParametar(string lokacija, DateTime datum, Parametri p)
        {
            DataProvider.ObrisiParametar(lokacija, datum);
            DataProvider.DodajParametar(p);
        }

        public static Parametri ProsecnaVrednost(string lokacija, DateTime start, DateTime kraj)
        {
            ISession session = SessionManager.GetSession();
            List<Parametri> parametri = new List<Parametri>();

            if (session == null)
                return null;

            parametri = DataProvider.PribaviParametre(lokacija, start, kraj);

            if (parametri.Count != 0)
            {
                float avgBrzinaV = 0, avgSneg = 0, avgTemp = 0, avgVazdusniP = 0, avgVlaznostV = 0, avgUV = 0;
                int avgKisa = 0, avgOblacnost = 0;
                Dictionary<string, int> smer = new Dictionary<string, int>();
                foreach (Parametri param in parametri)
                {
                    avgBrzinaV += param.BrzinaVetra;
                    avgSneg += param.Sneg;
                    avgTemp += param.Temperatura;
                    avgVazdusniP += param.VazdusniPritisak;
                    avgVlaznostV += param.VlaznostVazduha;
                    avgKisa += param.Kisa;
                    avgOblacnost += param.Oblacnost;
                    avgUV += param.UVIndeks;
                    if (!smer.ContainsKey(param.PravacVetra))
                    {
                        smer.Add(param.PravacVetra, 1);
                    }
                    else
                    {
                        smer[param.PravacVetra]++;
                    }
                }

                int n = parametri.Count;
                smer.OrderBy(x => x.Value);
                Parametri p = new Parametri(lokacija, start, avgBrzinaV / n, avgKisa / n, avgOblacnost / n, smer.First().Key, avgSneg / n, avgTemp / n, avgUV / n, avgVazdusniP / n, avgVlaznostV / n);
                return p;
            }
            else
                return null;
        }

        #endregion

        #region DBInicijalizacija

        public static void GenerisiPodatke()
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            DateTime datum = new DateTime(2019, 5, 5, 1, 0, 0);
            Random rnd = new Random();
            string[] niz = new string[8];
            niz[0] = "sever";
            niz[1] = "jug";
            niz[2] = "istok";
            niz[3] = "zapad";
            niz[4] = "severozapad";
            niz[5] = "severoistok";
            niz[6] = "jugoistok";
            niz[7] = "jugozapad";

            for (int i = 0; i < 10; i++)
            {
                Parametri p = new Parametri("Prokuplje", datum, (float)(rnd.NextDouble() * 100), rnd.Next(0, 100), rnd.Next(0, 100), niz[rnd.Next(0, 7)], (float)(rnd.NextDouble() * 100), (float)(rnd.NextDouble() * 100 - 50), (float)(rnd.NextDouble() * 15), (float)(rnd.NextDouble() * 1000 + 500), (float)(rnd.NextDouble() * 100));
                DataProvider.DodajParametar(p);
                datum = datum.AddHours(1);
            }
        }

        #endregion

        #region Lokacije

        private static List<string> CitajSveLokacije()
        {
            List<string> lokacije = new List<string>();

            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            var podaci = session.Execute("select * from \"lokacije\"");

            if (podaci != null)
            {
                foreach (var lokacija in podaci)
                {
                    string lok = lokacija["LOKACIJA"] != null ? lokacija["LOKACIJA"].ToString() : String.Empty;

                    lokacije.Add(lok);
                }

                return lokacije;
            }
            else
                return null;
        }

        private static void DodajLokaciju(string lokacija)
        {
            List<string> lokacije = CitajSveLokacije();
            if (lokacije.Contains(lokacija))
            {
                AzurirajLokaciju(lokacija);
                return;
            }
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;


            RowSet lok = session.Execute("insert into \"lokacije\" (\"LOKACIJA\",\"BROJ_PONAVLJANJA\")  values ('" + lokacija + "' ," + 1 + ")");

        }

        private static void AzurirajLokaciju(string lokacija)
        {
            string lok;
            int broj;

            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var podaci = session.Execute("select * from \"lokacije\" where \"LOKACIJA\"= '" + lokacija + "'");
            if (podaci != null)
            {
                var param = podaci.ElementAt(0);
                lok = param["LOKACIJA"].ToString();
                broj = (int)param["BROJ_PONAVLJANJA"];
                broj++;
                RowSet pod = session.Execute("update \"lokacije\" set \"BROJ_PONAVLJANJA\" = " + broj + " where \"LOKACIJA\"= '" + lok + "'");
            }
        }

        private static void ObrisiLokacije(string lokacija)
        {
            string lok;
            int broj;
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var podaci = session.Execute("select * from \"lokacije\" where \"LOKACIJA\"= '" + lokacija + "'");
            if (podaci != null)
            {
                var param = podaci.ElementAt(0);
                lok = param["LOKACIJA"].ToString();
                broj = (int)param["BROJ_PONAVLJANJA"];
                if (broj == 1)
                {
                    RowSet pod = session.Execute("delete from \"lokacije\" where \"LOKACIJA\"= '" + lok + "'");
                }
                else
                {
                    broj--;
                    RowSet pod = session.Execute("update \"lokacije\" set \"BROJ_PONAVLJANJA\" = " + broj + " where \"LOKACIJA\"= '" + lok + "'");
                }
            }

        }

        #endregion

        #region ProsecneVrednosti

        public static ProsecneVrednosti PribaviProsecnuVrednost(string lokacija)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            Row parametar = session.Execute("select * from \"prosecne_vrednosti\" where \"LOKACIJA\"='" + lokacija + "'").FirstOrDefault();

            if (parametar != null)
            {
                ProsecneVrednosti p = new ProsecneVrednosti();
                p.Lokacija = parametar["LOKACIJA"] != null ? parametar["LOKACIJA"].ToString() : String.Empty;
                p.BrzinaVetra = (float)parametar["BRZINA_VETRA"];
                p.Kisa = (int)parametar["KISA"];
                p.Oblacnost = (int)parametar["OBLACNOST"];
                p.Sneg = (float)parametar["SNEG"];
                p.Temperatura = (float)parametar["TEMPERATURA"];
                p.UVIndeks = (float)parametar["UV_INDEKS"];
                p.VazdusniPritisak = (float)parametar["VAZDUSNI_PRITISAK"];
                p.VlaznostVazduha = (float)parametar["VLAZNOST_VAZDUHA"];
                p.BrojMerenja = (double)parametar["BROJ_MERENJA"];
                return p;
            }
            else
                return null;


        }

        public static void DodajNovuProsecnuVrednost(ProsecneVrednosti prosVr)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet parametri = session.Execute("delete from \"prosecne_vrednosti\" where \"LOKACIJA\" = '" + prosVr.Lokacija + "'");

            DodajProsecnuVrednost(prosVr);
        }

        public static void DodajProsecnuVrednost(ProsecneVrednosti p)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet podaci = session.Execute("insert into \"prosecne_vrednosti\" (\"LOKACIJA\",\"BROJ_MERENJA\", \"BRZINA_VETRA\", \"KISA\", \"OBLACNOST\", \"SNEG\", \"TEMPERATURA\", \"UV_INDEKS\",\"VAZDUSNI_PRITISAK\", \"VLAZNOST_VAZDUHA\")  values ('" + p.Lokacija + "', " + p.BrojMerenja + "," + p.BrzinaVetra + ", " + p.Kisa + ", " + p.Oblacnost + ", " + p.Sneg + ", " + p.Temperatura + ", " + p.UVIndeks + "," + p.VazdusniPritisak + ", " + p.VlaznostVazduha + ")");

        }

        public static void ProsecneVrednostiNoviParametar(Parametri param)
        {
            ProsecneVrednosti prosVr = PribaviProsecnuVrednost(param.Lokacija);

            ProsecneVrednosti novaPV = new ProsecneVrednosti();

            if (prosVr == null)
            {
                novaPV.Lokacija = param.Lokacija;
                novaPV.BrzinaVetra = param.BrzinaVetra;
                novaPV.Kisa = param.Kisa;
                novaPV.Oblacnost = param.Oblacnost;
                novaPV.Sneg = param.Sneg;
                novaPV.Temperatura = param.Temperatura;
                novaPV.UVIndeks = param.UVIndeks;
                novaPV.VazdusniPritisak = param.VazdusniPritisak;
                novaPV.VlaznostVazduha = param.VlaznostVazduha;
                novaPV.BrojMerenja = 1;

                DodajNovuProsecnuVrednost(novaPV);
                return;
            }

            novaPV.Lokacija = prosVr.Lokacija;
            novaPV.BrojMerenja = prosVr.BrojMerenja + 1;
            novaPV.BrzinaVetra = (float)((prosVr.BrzinaVetra * prosVr.BrojMerenja + param.BrzinaVetra) / novaPV.BrojMerenja);
            novaPV.Kisa = (int)((prosVr.Kisa * prosVr.BrojMerenja + param.Kisa) / novaPV.BrojMerenja);
            novaPV.Oblacnost = (int)((prosVr.Oblacnost * prosVr.BrojMerenja + param.Oblacnost) / novaPV.BrojMerenja);
            novaPV.Sneg = (float)((prosVr.Sneg * prosVr.BrojMerenja + param.Sneg) / novaPV.BrojMerenja);
            novaPV.Temperatura = (float)((prosVr.Temperatura * prosVr.BrojMerenja + param.Temperatura) / novaPV.BrojMerenja);
            novaPV.UVIndeks = (float)((prosVr.UVIndeks * prosVr.BrojMerenja + param.UVIndeks) / novaPV.BrojMerenja);
            novaPV.VazdusniPritisak = (float)((prosVr.VazdusniPritisak * prosVr.BrojMerenja + param.VazdusniPritisak) / novaPV.BrojMerenja);
            novaPV.VlaznostVazduha = (float)((prosVr.VlaznostVazduha * prosVr.BrojMerenja + param.VlaznostVazduha) / novaPV.BrojMerenja);

            DodajNovuProsecnuVrednost(novaPV);

        }

        public static void ProsecneVrednostiBrisanjeParametra(Parametri param)
        {
            ProsecneVrednosti prosVr = PribaviProsecnuVrednost(param.Lokacija);

            ProsecneVrednosti novaPV = new ProsecneVrednosti();
            novaPV.Lokacija = prosVr.Lokacija;
            novaPV.BrojMerenja = prosVr.BrojMerenja - 1;
            novaPV.BrzinaVetra = (float)((prosVr.BrzinaVetra * prosVr.BrojMerenja - param.BrzinaVetra) / novaPV.BrojMerenja);
            novaPV.Kisa = (int)((prosVr.Kisa * prosVr.BrojMerenja - param.Kisa) / novaPV.BrojMerenja);
            novaPV.Oblacnost = (int)((prosVr.Oblacnost * prosVr.BrojMerenja - param.Oblacnost) / novaPV.BrojMerenja);
            novaPV.Sneg = (float)((prosVr.Sneg * prosVr.BrojMerenja - param.Sneg) / novaPV.BrojMerenja);
            novaPV.Temperatura = (float)((prosVr.Temperatura * prosVr.BrojMerenja - param.Temperatura) / novaPV.BrojMerenja);
            novaPV.UVIndeks = (float)((prosVr.UVIndeks * prosVr.BrojMerenja - param.UVIndeks) / novaPV.BrojMerenja);
            novaPV.VazdusniPritisak = (float)((prosVr.VazdusniPritisak * prosVr.BrojMerenja - param.VazdusniPritisak) / novaPV.BrojMerenja);
            novaPV.VlaznostVazduha = (float)((prosVr.VlaznostVazduha * prosVr.BrojMerenja - param.VlaznostVazduha) / novaPV.BrojMerenja);

            DodajNovuProsecnuVrednost(novaPV);
        }


        #endregion


        #region EkstremiTemperatura

        public static TemperaturaEkstremi PribaviTempEkstrem(string lokacija)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            Row parametar = session.Execute("select * from \"temperatura_ekstremi\" where \"LOKACIJA\"='" + lokacija + "'").FirstOrDefault();

            if (parametar != null)
            {
                TemperaturaEkstremi p = new TemperaturaEkstremi();
                p.Lokacija = parametar["LOKACIJA"] != null ? parametar["LOKACIJA"].ToString() : String.Empty;
                p.MaxTemperatura = (float)parametar["MAX_TEMPERATURA"];
                p.MinTemperatura = (float)parametar["MIN_TEMPERATURA"];
                return p;
            }
            else
                return null;
        }

        public static void DodajTempEkstrem(TemperaturaEkstremi p)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet parametarPodaci = session.Execute("insert into \"temperatura_ekstremi\" (\"LOKACIJA\", \"MAX_TEMPERATURA\", \"MIN_TEMPERATURA\")  values ('" + p.Lokacija + "', " + p.MaxTemperatura + ", " + p.MinTemperatura + ")");
        }

        public static void IzbrisiTempEkstrem(string lokacija)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet parametri = session.Execute("delete from \"temperatura_ekstremi\" where \"LOKACIJA\" = '" + lokacija + "'");

        }

        public static void TempEkstremiDodavanjeParametra(Parametri param)
        {
            TemperaturaEkstremi tempE = PribaviTempEkstrem(param.Lokacija);

            if (tempE == null)
            {
                tempE = new TemperaturaEkstremi();
                tempE.Lokacija = param.Lokacija;
                tempE.MaxTemperatura = param.Temperatura;
                tempE.MinTemperatura = param.Temperatura;
                DodajTempEkstrem(tempE);
                return;
            }

            if (tempE.MaxTemperatura < param.Temperatura)
            {
                TemperaturaEkstremi noviE = new TemperaturaEkstremi(tempE.Lokacija, param.Temperatura, tempE.MinTemperatura);
                IzbrisiTempEkstrem(tempE.Lokacija);
                DodajTempEkstrem(noviE);
            }

            if (tempE.MinTemperatura > param.Temperatura)
            {
                TemperaturaEkstremi noviE = new TemperaturaEkstremi(tempE.Lokacija, tempE.MaxTemperatura, param.Temperatura);
                IzbrisiTempEkstrem(tempE.Lokacija);
                DodajTempEkstrem(noviE);
            }

        }


        public static void TempEkstremiBrisanjeParametra(Parametri param)
        {
            TemperaturaEkstremi tempE = PribaviTempEkstrem(param.Lokacija);

            if (tempE.MaxTemperatura == param.Temperatura)
            {
                IzbrisiTempEkstrem(tempE.Lokacija);
                tempE.MaxTemperatura = NadjiMaksTemperaturu(param.Lokacija);

                DodajTempEkstrem(tempE);
            }
            if (tempE.MinTemperatura == param.Temperatura)
            {
                IzbrisiTempEkstrem(tempE.Lokacija);

                tempE.MinTemperatura = NadjiMinTemperaturu(param.Lokacija);

                DodajTempEkstrem(tempE);
            }

        }

        private static float NadjiMaksTemperaturu(string lokacija)
        {
            float maxTemp;
            List<Parametri> parametri = PribaviParametrePartitionKey(lokacija);
            maxTemp = parametri.First<Parametri>().Temperatura;

            foreach (Parametri p in parametri)
            {
                if (p.Temperatura > maxTemp)
                {
                    maxTemp = p.Temperatura;
                }
            }
            return maxTemp;
        }

        private static float NadjiMinTemperaturu(string lokacija)
        {
            float minTemp;
            List<Parametri> parametri = PribaviParametrePartitionKey(lokacija);
            minTemp = parametri.First<Parametri>().Temperatura;

            foreach (Parametri p in parametri)
            {
                if (p.Temperatura < minTemp)
                {
                    minTemp = p.Temperatura;
                }
            }
            return minTemp;
        }

        #endregion
    }
}

