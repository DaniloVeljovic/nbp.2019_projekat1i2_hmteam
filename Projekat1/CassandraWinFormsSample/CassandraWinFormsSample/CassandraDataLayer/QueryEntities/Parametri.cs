﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Parametri
    {
        public string Lokacija { get; set; }
        public DateTime DatumIVreme { get; set; }
        public float BrzinaVetra { get; set; }
        public int Kisa { get; set; }
        public int Oblacnost { get; set; }
        public string PravacVetra { get; set; }
        public float Sneg { get; set; }
        public float Temperatura { get; set; }
        public float UVIndeks { get; set; }
        public float VazdusniPritisak { get; set; }
        public float VlaznostVazduha { get; set; }

        public Parametri()
        {

        }
        public Parametri(string lokacija,DateTime datum,float brzinaV, int kisa, int oblacnost,string pravacV, float sneg,float temperatura,float UV,float vazdusniP,float vlaznostV)
        {
            this.Lokacija = lokacija;
            this.DatumIVreme = datum;
            this.BrzinaVetra = brzinaV;
            this.Kisa = kisa;
            this.Oblacnost = oblacnost;
            this.PravacVetra = pravacV;
            this.Sneg = sneg;
            this.Temperatura = temperatura;
            this.UVIndeks = UV;
            this.VazdusniPritisak = vazdusniP;
            this.VlaznostVazduha = vlaznostV;
        }
    }
}
