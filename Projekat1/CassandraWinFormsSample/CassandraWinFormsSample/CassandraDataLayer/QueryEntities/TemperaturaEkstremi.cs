﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class TemperaturaEkstremi
    {
        public string Lokacija { get; set; }
        public float MaxTemperatura { get; set; }
        public float MinTemperatura { get; set; }
        public TemperaturaEkstremi() { }

        public TemperaturaEkstremi(string lok, float maxT,float minT)
        {
            this.Lokacija = lok;
            this.MaxTemperatura = maxT;
            this.MinTemperatura = minT;
        }
    }
}
