﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class ProsecneVrednosti
    {
        public string Lokacija { get; set; }
        public float BrzinaVetra { get; set; }
        public int Kisa { get; set; }
        public int Oblacnost { get; set; }
        public float Sneg { get; set; }
        public float Temperatura { get; set; }
        public float UVIndeks { get; set; }
        public float VazdusniPritisak { get; set; }
        public float VlaznostVazduha { get; set; }
        public double BrojMerenja { get; set; }

        public ProsecneVrednosti()
        {

        }
        public ProsecneVrednosti(string lokacija, float brzinaV, int kisa, int oblacnost, float sneg, float temperatura, float UV, float vazdusniP, float vlaznostV,double brojM)
        {
            this.Lokacija = lokacija;
            this.BrzinaVetra = brzinaV;
            this.Kisa = kisa;
            this.Oblacnost = oblacnost;
            this.Sneg = sneg;
            this.Temperatura = temperatura;
            this.UVIndeks = UV;
            this.VazdusniPritisak = vazdusniP;
            this.VlaznostVazduha = vlaznostV;
            this.BrojMerenja = brojM;
        }
    }

}
