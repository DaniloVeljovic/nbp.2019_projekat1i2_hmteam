﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace CassandraWinFormsSample
{
    public partial class FormAzuriranje : Form
    {
        public FormAzuriranje()
        {
            InitializeComponent();
        }
        
        public FormAzuriranje(Parametri p)
        {
            InitializeComponent();
            NapuniKomponente(p);
        }

        private void tbGradA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ' ')
            {
                e.Handled = true;
            }
        }

        private void tbBrzinaVetra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void tbKisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbOblacnost_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbTemperatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void tbSneg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void tbUV_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void tbVazdusniPritisak_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void tbVlaznostVazduha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void NapuniKomponente(Parametri p)
        {
            tbGradA.Text = p.Lokacija;
            dtpDatumA.Value = p.DatumIVreme;
            tbBrzinaVetra.Text = p.BrzinaVetra.ToString();
            tbKisa.Text = p.Kisa.ToString();
            tbOblacnost.Text = p.Oblacnost.ToString();
            cbPravacVetra.Text = p.PravacVetra;
            tbSneg.Text = p.Sneg.ToString();
            tbTemperatura.Text = p.Temperatura.ToString();
            tbUV.Text = p.UVIndeks.ToString();
            tbVazdusniPritisak.Text = p.VazdusniPritisak.ToString();
            tbVlaznostVazduha.Text = p.VlaznostVazduha.ToString();
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            Parametri p = new Parametri();
            p.Lokacija = tbGradA.Text;
            p.DatumIVreme = dtpDatumA.Value;
            p.BrzinaVetra = float.Parse(tbBrzinaVetra.Text);
            p.Kisa = int.Parse(tbKisa.Text);
            p.Oblacnost = int.Parse(tbOblacnost.Text);
            p.PravacVetra = cbPravacVetra.Text;
            p.Sneg = float.Parse(tbSneg.Text);
            p.Temperatura = float.Parse(tbTemperatura.Text);
            p.UVIndeks = float.Parse(tbUV.Text);
            p.VazdusniPritisak = float.Parse(tbVazdusniPritisak.Text);
            p.VlaznostVazduha = float.Parse(tbVlaznostVazduha.Text);

            DataProvider.AzurirajParametar(p.Lokacija, p.DatumIVreme, p);
            this.Close();
        }
    }
}
