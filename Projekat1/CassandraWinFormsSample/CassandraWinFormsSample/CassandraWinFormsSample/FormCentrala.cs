﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace CassandraWinFormsSample
{
    public partial class FormCentrala : Form
    {
        public FormCentrala()
        {
            InitializeComponent();
            dtpDatumA.Value = dtpDatumD.Value=dtpDatumO.Value= DateTime.Parse("2000-05-20 00:00:00.000+0000");
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbGradD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ' ')
            {
                e.Handled = true;
            }
        }

        private void tbBrzinaVetra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void tbKisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbOblacnost_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbTemperatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar!='-')
            {
                e.Handled = true;
            }
        }

        private void tbSneg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void tbUV_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void tbVlaznostVazduha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            Parametri p = new Parametri();
            p.Lokacija = tbGradD.Text;
            p.DatumIVreme = dtpDatumD.Value;
            p.BrzinaVetra = float.Parse(tbBrzinaVetra.Text);
            p.Kisa = int.Parse(tbKisa.Text);
            p.Oblacnost = int.Parse(tbOblacnost.Text);
            p.PravacVetra = cbPravacVetra.Text;
            p.Sneg = float.Parse(tbSneg.Text);
            p.Temperatura = float.Parse(tbTemperatura.Text);
            p.UVIndeks = float.Parse(tbUV.Text);
            p.VazdusniPritisak = float.Parse(tbVazdusniPritisak.Text);
            p.VlaznostVazduha = float.Parse(tbVlaznostVazduha.Text);

            DataProvider.DodajParametar(p);
        }

        private void tbGradO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ' ')
            {
                e.Handled = true;
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {

            
            DataProvider.ObrisiParametar(tbGradO.Text, dtpDatumO.Value);
        }

        private void tbGradA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ' ')
            {
                e.Handled = true;
            }
        }

        private void tbVazdusniPritisak_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            Parametri p = new Parametri();
            p = DataProvider.PribaviParametar(tbGradA.Text, dtpDatumA.Value);

            Form f = new FormAzuriranje(p);
            f.ShowDialog();
        }

        private void FormCentrala_Load(object sender, EventArgs e)
        {
            DataProvider.GenerisiPodatke();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
