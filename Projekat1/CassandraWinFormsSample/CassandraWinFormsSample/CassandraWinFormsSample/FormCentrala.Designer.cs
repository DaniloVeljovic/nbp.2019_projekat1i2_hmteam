﻿namespace CassandraWinFormsSample
{
    partial class FormCentrala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbVlaznostVazduha = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbVazdusniPritisak = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbUV = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbSneg = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbTemperatura = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbOblacnost = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbPravacVetra = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbKisa = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbBrzinaVetra = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDatumD = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tbGradD = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.dtpDatumO = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.tbGradO = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnAzuriraj = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.dtpDatumA = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.tbGradA = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tbVlaznostVazduha);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbVazdusniPritisak);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbUV);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbSneg);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbTemperatura);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbOblacnost);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbPravacVetra);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbKisa);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbBrzinaVetra);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpDatumD);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbGradD);
            this.groupBox1.Location = new System.Drawing.Point(206, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1131, 351);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Unos podataka";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel2.Controls.Add(this.btnDodaj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(3, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(185, 325);
            this.panel2.TabIndex = 30;
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnDodaj.FlatAppearance.BorderSize = 0;
            this.btnDodaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodaj.ForeColor = System.Drawing.Color.White;
            this.btnDodaj.Location = new System.Drawing.Point(0, 119);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(185, 53);
            this.btnDodaj.TabIndex = 29;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = false;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(546, 286);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 21);
            this.label18.TabIndex = 28;
            this.label18.Text = "mbar";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(775, 286);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 21);
            this.label17.TabIndex = 27;
            this.label17.Text = "%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(775, 217);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 21);
            this.label16.TabIndex = 26;
            this.label16.Text = "cm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(546, 219);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 21);
            this.label15.TabIndex = 25;
            this.label15.Text = "°C";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(327, 217);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 21);
            this.label14.TabIndex = 24;
            this.label14.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(775, 145);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 21);
            this.label13.TabIndex = 23;
            this.label13.Text = "mm/m²";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(327, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 21);
            this.label12.TabIndex = 22;
            this.label12.Text = "m/s";
            // 
            // tbVlaznostVazduha
            // 
            this.tbVlaznostVazduha.Location = new System.Drawing.Point(669, 283);
            this.tbVlaznostVazduha.Name = "tbVlaznostVazduha";
            this.tbVlaznostVazduha.Size = new System.Drawing.Size(100, 27);
            this.tbVlaznostVazduha.TabIndex = 21;
            this.tbVlaznostVazduha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVlaznostVazduha_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(666, 263);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 21);
            this.label11.TabIndex = 20;
            this.label11.Text = "Vlaznost vazduha:";
            // 
            // tbVazdusniPritisak
            // 
            this.tbVazdusniPritisak.Location = new System.Drawing.Point(440, 283);
            this.tbVazdusniPritisak.Name = "tbVazdusniPritisak";
            this.tbVazdusniPritisak.Size = new System.Drawing.Size(100, 27);
            this.tbVazdusniPritisak.TabIndex = 19;
            this.tbVazdusniPritisak.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVazdusniPritisak_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(437, 263);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 21);
            this.label10.TabIndex = 18;
            this.label10.Text = "Vazdusni pritisak:";
            // 
            // tbUV
            // 
            this.tbUV.Location = new System.Drawing.Point(221, 283);
            this.tbUV.MaxLength = 5;
            this.tbUV.Name = "tbUV";
            this.tbUV.Size = new System.Drawing.Size(100, 27);
            this.tbUV.TabIndex = 17;
            this.tbUV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbUV_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(218, 263);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 21);
            this.label9.TabIndex = 16;
            this.label9.Text = "UV indeks:";
            // 
            // tbSneg
            // 
            this.tbSneg.Location = new System.Drawing.Point(669, 214);
            this.tbSneg.MaxLength = 7;
            this.tbSneg.Name = "tbSneg";
            this.tbSneg.Size = new System.Drawing.Size(100, 27);
            this.tbSneg.TabIndex = 15;
            this.tbSneg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSneg_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(666, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 21);
            this.label8.TabIndex = 14;
            this.label8.Text = "Sneg:";
            // 
            // tbTemperatura
            // 
            this.tbTemperatura.Location = new System.Drawing.Point(440, 214);
            this.tbTemperatura.MaxLength = 5;
            this.tbTemperatura.Name = "tbTemperatura";
            this.tbTemperatura.Size = new System.Drawing.Size(100, 27);
            this.tbTemperatura.TabIndex = 13;
            this.tbTemperatura.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.tbTemperatura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTemperatura_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(437, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 21);
            this.label7.TabIndex = 12;
            this.label7.Text = "Temperatura:";
            // 
            // tbOblacnost
            // 
            this.tbOblacnost.Location = new System.Drawing.Point(221, 214);
            this.tbOblacnost.MaxLength = 3;
            this.tbOblacnost.Name = "tbOblacnost";
            this.tbOblacnost.Size = new System.Drawing.Size(100, 27);
            this.tbOblacnost.TabIndex = 11;
            this.tbOblacnost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbOblacnost_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(218, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 21);
            this.label6.TabIndex = 10;
            this.label6.Text = "Oblacnost:";
            // 
            // cbPravacVetra
            // 
            this.cbPravacVetra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPravacVetra.FormattingEnabled = true;
            this.cbPravacVetra.Items.AddRange(new object[] {
            "sever",
            "jug",
            "istok",
            "zapad",
            "severozapad",
            "severoistok",
            "jugozapad",
            "jugoistok"});
            this.cbPravacVetra.Location = new System.Drawing.Point(440, 140);
            this.cbPravacVetra.Name = "cbPravacVetra";
            this.cbPravacVetra.Size = new System.Drawing.Size(121, 29);
            this.cbPravacVetra.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(437, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "Pravac vetra:";
            // 
            // tbKisa
            // 
            this.tbKisa.Location = new System.Drawing.Point(669, 142);
            this.tbKisa.MaxLength = 4;
            this.tbKisa.Name = "tbKisa";
            this.tbKisa.Size = new System.Drawing.Size(100, 27);
            this.tbKisa.TabIndex = 7;
            this.tbKisa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKisa_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(666, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Kisa:";
            // 
            // tbBrzinaVetra
            // 
            this.tbBrzinaVetra.Location = new System.Drawing.Point(222, 145);
            this.tbBrzinaVetra.MaxLength = 5;
            this.tbBrzinaVetra.Name = "tbBrzinaVetra";
            this.tbBrzinaVetra.Size = new System.Drawing.Size(100, 27);
            this.tbBrzinaVetra.TabIndex = 5;
            this.tbBrzinaVetra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBrzinaVetra_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(218, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Brzina vetra:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(437, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "Datum i vreme:";
            // 
            // dtpDatumD
            // 
            this.dtpDatumD.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpDatumD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumD.Location = new System.Drawing.Point(440, 72);
            this.dtpDatumD.Name = "dtpDatumD";
            this.dtpDatumD.Size = new System.Drawing.Size(146, 27);
            this.dtpDatumD.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(218, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Grad:";
            // 
            // tbGradD
            // 
            this.tbGradD.Location = new System.Drawing.Point(222, 72);
            this.tbGradD.Name = "tbGradD";
            this.tbGradD.Size = new System.Drawing.Size(100, 27);
            this.tbGradD.TabIndex = 0;
            this.tbGradD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbGradD_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.dtpDatumO);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.tbGradO);
            this.groupBox2.Location = new System.Drawing.Point(206, 413);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1131, 168);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Brisanje podataka";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel3.Controls.Add(this.btnObrisi);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(185, 142);
            this.panel3.TabIndex = 9;
            // 
            // btnObrisi
            // 
            this.btnObrisi.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnObrisi.FlatAppearance.BorderSize = 0;
            this.btnObrisi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObrisi.ForeColor = System.Drawing.Color.White;
            this.btnObrisi.Location = new System.Drawing.Point(0, 56);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(185, 38);
            this.btnObrisi.TabIndex = 8;
            this.btnObrisi.Text = "Obrisi";
            this.btnObrisi.UseVisualStyleBackColor = false;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(436, 70);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 21);
            this.label19.TabIndex = 7;
            this.label19.Text = "Datum i vreme:";
            // 
            // dtpDatumO
            // 
            this.dtpDatumO.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpDatumO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumO.Location = new System.Drawing.Point(440, 94);
            this.dtpDatumO.Name = "dtpDatumO";
            this.dtpDatumO.Size = new System.Drawing.Size(146, 27);
            this.dtpDatumO.TabIndex = 6;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(217, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 21);
            this.label20.TabIndex = 5;
            this.label20.Text = "Grad:";
            // 
            // tbGradO
            // 
            this.tbGradO.Location = new System.Drawing.Point(221, 94);
            this.tbGradO.Name = "tbGradO";
            this.tbGradO.Size = new System.Drawing.Size(100, 27);
            this.tbGradO.TabIndex = 4;
            this.tbGradO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbGradO_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.panel4);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.dtpDatumA);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.tbGradA);
            this.groupBox3.Location = new System.Drawing.Point(206, 602);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1131, 135);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Azuriranje podataka";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel4.Controls.Add(this.btnAzuriraj);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(3, 23);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(185, 109);
            this.panel4.TabIndex = 14;
            // 
            // btnAzuriraj
            // 
            this.btnAzuriraj.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAzuriraj.FlatAppearance.BorderSize = 0;
            this.btnAzuriraj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAzuriraj.ForeColor = System.Drawing.Color.White;
            this.btnAzuriraj.Location = new System.Drawing.Point(0, 30);
            this.btnAzuriraj.Name = "btnAzuriraj";
            this.btnAzuriraj.Size = new System.Drawing.Size(185, 43);
            this.btnAzuriraj.TabIndex = 13;
            this.btnAzuriraj.Text = "Azuriraj";
            this.btnAzuriraj.UseVisualStyleBackColor = false;
            this.btnAzuriraj.Click += new System.EventHandler(this.btnAzuriraj_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(437, 53);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(130, 21);
            this.label21.TabIndex = 12;
            this.label21.Text = "Datum i vreme:";
            // 
            // dtpDatumA
            // 
            this.dtpDatumA.CustomFormat = "yyyy-MM-dd hh:00:00.000+0000";
            this.dtpDatumA.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumA.Location = new System.Drawing.Point(440, 77);
            this.dtpDatumA.Name = "dtpDatumA";
            this.dtpDatumA.Size = new System.Drawing.Size(146, 27);
            this.dtpDatumA.TabIndex = 11;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(218, 53);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 21);
            this.label22.TabIndex = 10;
            this.label22.Text = "Grad:";
            // 
            // tbGradA
            // 
            this.tbGradA.Location = new System.Drawing.Point(222, 77);
            this.tbGradA.Name = "tbGradA";
            this.tbGradA.Size = new System.Drawing.Size(100, 27);
            this.tbGradA.TabIndex = 9;
            this.tbGradA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbGradA_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.btnZatvori);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 749);
            this.panel1.TabIndex = 30;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CassandraWinFormsSample.Properties.Resources.final;
            this.pictureBox1.Location = new System.Drawing.Point(48, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnZatvori
            // 
            this.btnZatvori.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnZatvori.FlatAppearance.BorderSize = 0;
            this.btnZatvori.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZatvori.ForeColor = System.Drawing.Color.White;
            this.btnZatvori.Location = new System.Drawing.Point(0, 312);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(200, 50);
            this.btnZatvori.TabIndex = 31;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = false;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // FormCentrala
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1061, 749);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCentrala";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Centrala";
            this.Load += new System.EventHandler(this.FormCentrala_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbTemperatura;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbOblacnost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbPravacVetra;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbKisa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbBrzinaVetra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDatumD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbGradD;
        private System.Windows.Forms.TextBox tbSneg;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbVlaznostVazduha;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbVazdusniPritisak;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbUV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtpDatumO;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbGradO;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAzuriraj;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dtpDatumA;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbGradA;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnZatvori;
    }
}